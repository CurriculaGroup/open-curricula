---
title: Pulmonary Volumes & Capacities
youtube_id: ndf7Mn_eB0I
tags:
    - SSC
    - BIOLOGY
    - Respiration
categories:
    - Gaseous Exchange
---
