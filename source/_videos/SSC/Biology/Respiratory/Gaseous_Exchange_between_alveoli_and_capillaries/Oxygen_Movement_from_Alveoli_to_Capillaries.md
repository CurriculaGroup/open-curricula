---
title: Oxygen Movement from Alveoli to Capillaries
youtube_id: nRpwdwm06Ic
tags:
    - SSC
    - BIOLOGY
    - Respiration
categories:
    - Gaseous Exchange
---
