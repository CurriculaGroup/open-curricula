---
title: Gaseous exchange between alveoli and capillaries
youtube_id: XTMYSGXhJ4E
tags:
    - SSC
    - BIOLOGY
    - Respiration
categories:
    - Gaseous Exchange
---
