---
title: The correlation between breathing and swallowing
youtube_id: BnUb0JK88a4
tags:
    - SSC
    - BIOLOGY
    - Respiration
categories:
    - air_pathway
---
